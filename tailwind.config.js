/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './templates/**/*.html',
    './static/**/*.js',
    './static/**/*.css',
    // เพิ่ม paths อื่นๆ ตามความจำเป็น
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};

