# middleware.py
from django.conf import settings
from django.http import HttpResponse

class MaintenanceModeMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # ตรวจสอบว่า Maintenance Mode เปิดอยู่หรือไม่
        if getattr(settings, 'MAINTENANCE_MODE', False):
            return HttpResponse('<h1>เว็บไซต์อยู่ระหว่างการบำรุงรักษา กรุณากลับมาใหม่ภายหลัง</h1>', status=503)
        return self.get_response(request)
