from django import forms
from django.forms import ModelForm
from .models import Profile
from allauth.account.forms import PasswordField, SignupForm

#login
from django.urls import NoReverseMatch, reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _, pgettext

from allauth.account.forms import LoginForm
from allauth.utils import get_username_max_length, set_form_field_order
from allauth.account import app_settings
from allauth.account.app_settings import AuthenticationMethod




class ProfileForm(ModelForm):
  class Meta:
    model = Profile
    fields = '__all__'
    exclude = ['user']
    labels = {
      'realname' : 'ชื่อ-นามสกุล',
      'bio' : 'คำอธิบาย'
    }
    widgets = {
      'image' : forms.FileInput(),
      'bio' : forms.Textarea(attrs={'rows':3})
    }
    
# class CustomSignupForm(SignupForm):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         # กำหนดข้อความ help_text สำหรับฟิลด์รหัสผ่านโดยตรง
#         self.fields['password1'].help_text = (
#             "รหัสผ่านของคุณต้องมีอย่างน้อย 8 ตัวอักษร ไม่ควรคล้ายกับข้อมูลส่วนตัวอื่น ๆ และไม่ควรใช้รหัสผ่านที่นิยมใช้กันทั่วไป"
#         )
#         self.fields['password2'].help_text = "กรุณายืนยันรหัสผ่านของคุณอีกครั้ง"

class MyCustomLoginForm(LoginForm):
    password = PasswordField(label=_("Password"), autocomplete="current-password")
    remember = forms.BooleanField(label=_("Remember Me"), required=False)

    user = None

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(LoginForm, self).__init__(*args, **kwargs)
        if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
            login_widget = forms.EmailInput(
                attrs={
                    "placeholder": _("Email address"),
                    "autocomplete": "email",
                }
            )
            login_field = forms.EmailField(label=_("Email"), widget=login_widget)
        elif app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.USERNAME:
            login_widget = forms.TextInput(
                attrs={"placeholder": _("Username"), "autocomplete": "username"}
            )
            login_field = forms.CharField(
                label=_("Username"),
                widget=login_widget,
                max_length=get_username_max_length(),
            )
        else:
            assert (
                app_settings.AUTHENTICATION_METHOD
                == AuthenticationMethod.USERNAME_EMAIL
            )
            login_widget = forms.TextInput(
                attrs={"placeholder": _("Username or email"), "autocomplete": "email"}
            )
            login_field = forms.CharField(
                label=pgettext("field label", "Login"), widget=login_widget
            )
        self.fields["login"] = login_field
        set_form_field_order(self, ["login", "password", "remember"])
        if app_settings.SESSION_REMEMBER is not None:
            del self.fields["remember"]
        try:
            reset_url = reverse("account_reset_password")
        except NoReverseMatch:
            pass
        else:
            forgot_txt = _("ลืมรหัสผ่าน?")
            self.fields["password"].help_text = mark_safe(
                f'<a href="{reset_url}">{forgot_txt}</a>'
            )
            

# class MyCustomLoginForm(LoginForm):
#     def login(self, *args, **kwargs):
#         password = PasswordField(label=_("Password"), autocomplete="current-password")
#         remember = forms.BooleanField(label=_("Remember Me"), required=False)

#         user = None
#         def __init__(self, *args, **kwargs):
#           self.request = kwargs.pop("request", None)
#           super(LoginForm, self).__init__(*args, **kwargs)
#           if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
#               login_widget = forms.EmailInput(
#                   attrs={
#                       "placeholder": _("Email address"),
#                       "autocomplete": "email",
#                   }
#               )
#               login_field = forms.EmailField(label=_("Email"), widget=login_widget)
#           elif app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.USERNAME:
#               login_widget = forms.TextInput(
#                   attrs={"placeholder": _("Username"), "autocomplete": "username"}
#               )
#               login_field = forms.CharField(
#                   label=_("Username"),
#                   widget=login_widget,
#                   max_length=get_username_max_length(),
#               )
#           else:
#               assert (
#                   app_settings.AUTHENTICATION_METHOD
#                   == AuthenticationMethod.USERNAME_EMAIL
#               )
#               login_widget = forms.TextInput(
#                   attrs={"placeholder": _("Username or email"), "autocomplete": "email"}
#               )
#               login_field = forms.CharField(
#                   label=pgettext("field label", "Login"), widget=login_widget
#               )
#           self.fields["login"] = login_field
#           set_form_field_order(self, ["login", "password", "remember"])
#           if app_settings.SESSION_REMEMBER is not None:
#               del self.fields["remember"]
#           try:
#               reset_url = reverse("account_reset_password")
#           except NoReverseMatch:
#               pass
#           else:
#               forgot_txt = _("ลืมรหัสผ่าน?")
#               self.fields["password"].help_text = mark_safe(
#                   f'<a href="{reset_url}">{forgot_txt}</a>'
#               )
#         return super(MyCustomLoginForm, self).login(*args, **kwargs)