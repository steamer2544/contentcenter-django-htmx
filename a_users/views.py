from pyexpat.errors import messages
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.urls import reverse

from a_contents.models import Content
from a_contents.utils.filters import ContentFilter
#page paginate
from django.core.paginator import Paginator
from .forms import *

# Create your views here.
def profile_view(request, username=None):
    if username != request.user.username and username:
        # print("userprofile_page")
        profile = get_object_or_404(User, username=username).profile
        user_content = Content.objects.filter(author=profile.user, approval_status=Content.ApprovalStatus.APPROVED)  #มี field 'author' ใน Content model ที่เชื่อมกับ User
    else:
        try:
            # print("profile_page")
            profile = request.user.profile
            user_content = Content.objects.filter(author=request.user)  #มี field 'author' ใน Content model ที่เชื่อมกับ User
        except User.profile.RelatedObjectDoesNotExist:
            raise Http404()
    
    # สร้าง content_filter ก่อนการจัดเรียง
    content_filter = ContentFilter(request.GET, queryset=user_content)
    
    # การจัดเรียงข้อมูลตาม sort_order
    sort_order = request.GET.get('sort', 'popular')
    if sort_order == 'popular':
        queryset = content_filter.qs.order_by('-view_count', '-likes')
    elif sort_order == 'latest':
        queryset = content_filter.qs.order_by('-created')
        
    # การตั้งค่า Pagination
    paginator = Paginator(queryset, 12)  # แสดง 12 รายการต่อหน้า
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'profile': profile,
        'contents': page_obj,
        'sort_order': sort_order,
    }
    # ใช้ HTMX เพื่อโหลด partials
    if request.htmx:
        return render(request, 'partials/userprofile_content_list.html', context)
    
    return render(request, 'a_users/profile.html', context)

@login_required
def profile_edit_view(request):
  form = ProfileForm(instance=request.user.profile)
  
  if request.method == 'POST':
    form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
    if form.is_valid():
      form.save()
      return redirect('profile')
    
  if request.path == reverse('profile-onboarding'):
    template = 'a_users/profile_onboarding.html'
  else:
    template = 'a_users/profile_edit.html'
  
  return render(request, template, {'form':form})

@login_required
def profile_delete_view(request):
  user = request.user
  if request.method == 'POST':
    logout(request)
    user.delete()
    messages.success(request, 'ลบบัญชีเสร็จสมบูรณ์.')
    return redirect('home')
  
  return render(request, 'a_users/profile_delete.html')