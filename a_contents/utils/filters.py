import django_filters
from a_contents.models import *
from django import forms

class ContentFilter(django_filters.FilterSet):
    grades__name = django_filters.ChoiceFilter(
        choices=Grade.objects.values_list('name', 'name'),
        empty_label='เลือกชั้นเรียน',  # แสดงเป็นค่าเริ่มต้นใน select dropdown
        widget=forms.Select(
            attrs={
                'class': 'form-select text-black rounded-full font-semibold',
            }
        )
    )
    learning_areas__name = django_filters.ChoiceFilter(
        choices=LearningArea.objects.values_list('name', 'name'),
        empty_label='เลือกกลุ่มสาระการเรียนรู้',  # แสดงเป็นค่าเริ่มต้นใน select dropdown
        widget=forms.Select(
            attrs={
                'class': 'form-select text-black rounded-full font-semibold',
            }
        )
    )
    title = django_filters.CharFilter(
        field_name='title',
        lookup_expr='icontains',
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-black rounded-full font-semibold',
                'placeholder': 'ค้นหาชื่อเนื้อหา'
            }
        )
    )

    class Meta:
        model = Content
        fields = {
            'title': ['icontains'],
            'type': ['exact'],
        }
        exclude = ['type']