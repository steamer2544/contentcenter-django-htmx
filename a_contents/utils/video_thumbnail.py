import re
import requests
from django.core.files.base import ContentFile
from django.http import JsonResponse

def get_youtube_thumbnail(url):
    # Regular expression pattern for extracting video ID from YouTube URL
    video_id_pattern = r'(?:v=|\/)([0-9A-Za-z_-]{11}).*'
    match = re.search(video_id_pattern, url)
    if match:
        video_id = match.group(1)
        thumbnail_url = f'https://img.youtube.com/vi/{video_id}/maxresdefault.jpg'
        # Check if thumbnail exists
        response = requests.head(thumbnail_url)
        if response.status_code == 200:
            return thumbnail_url
        else:
            # Fallback to another thumbnail quality if maxresdefault doesn't exist
            thumbnail_url = f'https://img.youtube.com/vi/{video_id}/hqdefault.jpg'
            return thumbnail_url
    else:
        return None


def get_video_thumbnail(request):
    video_url = request.GET.get('url')
    if video_url:
        thumbnail_url = get_youtube_thumbnail(video_url)
        if thumbnail_url:
            return JsonResponse({'thumbnail_url': thumbnail_url})
        else:
            return JsonResponse({'error': 'Invalid YouTube URL or thumbnail not found'}, status=400)
    else:
        return JsonResponse({'error': 'URL parameter is missing'}, status=400)
    
def save_thumbnail(content, video_url):
    thumbnail_url = get_youtube_thumbnail(video_url)
    if thumbnail_url:
        response = requests.get(thumbnail_url)
        if response.status_code == 200:
            file_name = f'{content.id}_thumbnail.jpg'
            content.image.save(file_name, ContentFile(response.content), save=False)
        else:
            print('Failed to download thumbnail')
    else:
        print('Failed to get thumbnail URL')
