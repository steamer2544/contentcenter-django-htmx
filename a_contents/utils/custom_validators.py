from django.core.validators import FileExtensionValidator
from django.core.files.images import get_image_dimensions

# ฟังก์ชันเพื่อตรวจสอบภาพ
def is_valid_image(image):
    # ตรวจสอบชนิดของไฟล์
    if not image.content_type.startswith('image'):
        return False, "ไฟล์ภาพไม่ถูกต้อง."

    # ตรวจสอบขนาดของไฟล์ (ขนาดต้องไม่เกิน 5MB)
    if image.size > 5 * 1024 * 1024:
        return False, "ขนาดไฟล์ใหญ่เกินไป ควรมีขนาดไม่เกิน 5MB"

    # ตรวจสอบขนาดของภาพ (ตัวอย่าง: ความกว้างไม่เกิน 3000 พิกเซล และความสูงไม่เกิน 3000 พิกเซล)
    width, height = get_image_dimensions(image)
    if width > 3000 or height > 3000:
        return False, "ขนาดรูปภาพใหญ่เกินไป ควรมีความกว้างไม่เกิน 3000 พิกเซล และความสูงไม่เกิน 3000 พิกเซล"

    return True, None

def is_valid_project_file(file):
    # ตรวจสอบชนิดของไฟล์
    if not file.name.endswith('.pdf'):
        return False, "ชนิดไฟล์ไม่ถูกต้อง กรุณาอัพโหลดไฟล์ PDF"

    # ตรวจสอบขนาดของไฟล์ (ขนาดต้องไม่เกิน 10MB)
    if file.size > 10 * 1024 * 1024:
        return False, "ขนาดไฟล์ใหญ่เกินไป ควรมีขนาดไม่เกิน 10MB"

    return True, None

def is_valid_plan_file(file):
    # ตรวจสอบชนิดของไฟล์
    if not file.name.endswith(('.pdf', '.doc', '.docx')):  # เปลี่ยนเป็นทูเพิล
        return False, "ชนิดไฟล์ไม่ถูกต้อง กรุณาอัพโหลดไฟล์ PDF, DOCX หรือ DOC"

    # ตรวจสอบขนาดของไฟล์ (ขนาดต้องไม่เกิน 10MB)
    if file.size > 10 * 1024 * 1024:
        return False, "ขนาดไฟล์ใหญ่เกินไป ควรมีขนาดไม่เกิน 10MB"

    return True, None
