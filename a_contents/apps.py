from django.apps import AppConfig


class AContentsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'a_contents'
