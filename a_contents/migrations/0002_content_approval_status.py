# Generated by Django 5.0.6 on 2024-08-21 22:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('a_contents', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='content',
            name='approval_status',
            field=models.CharField(choices=[('P', 'Pending'), ('A', 'Approved'), ('R', 'Rejected')], default='P', max_length=1),
        ),
    ]
