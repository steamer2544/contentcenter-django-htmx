from django.db import models

from django.contrib.auth.models import User
from django_ckeditor_5.fields import CKEditor5Field
from django.templatetags.static import static
import uuid

from embed_video.fields import EmbedVideoField
from django.core.validators import FileExtensionValidator


# Create your models here.
class Content(models.Model):
    class TypeChoices(models.TextChoices):
        VIDEO = 'V'
        GALLERY = 'G'
        ARTICLE = 'R'
        PROJECT = 'P'
        LESSON = 'L'
        EBOOK = 'E'
        APP = 'A'
        TEACHING_PLAN = 'T'
        
    class ApprovalStatus(models.TextChoices):
        PENDING = 'P', 'Pending'
        APPROVED = 'A', 'Approved'
        REJECTED = 'R', 'Rejected'
        
    title = models.CharField(max_length=500)
    type = models.CharField(max_length=1, choices=TypeChoices.choices)
    image = models.ImageField(upload_to='thumpnails/', null=True, blank=True) #ถ้าเชื่อได้ไวว่ามันเซฟไปที่มีเดียจริงๆก็ค่อยเป็นเป็น '/'
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='contents')
    description = models.TextField()
    
    likes = models.ManyToManyField(User, related_name="likecontents", through="LikedContent")
    view_count = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    id = models.CharField(max_length=100, default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    grades = models.ManyToManyField('Grade')
    learning_areas = models.ManyToManyField('LearningArea')
    subject = models.CharField(max_length=500, null=True, blank=True)
    
    # Approval status field
    approval_status = models.CharField(max_length=1, choices=ApprovalStatus.choices, default=ApprovalStatus.PENDING)
    
    
    def __str__(self):
        return self.title
    
    class Meta:
        ordering = ['-created']
        
    @property
    def thumbnail(self):
        try:
            thumbnail = self.image.url
        except:
            thumbnail = static('images/catbook3.jpg')
        return thumbnail
    
class LikedContent(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f'{self.user.username}: {self.content.title}'

class Grade(models.Model):
    name = models.CharField(max_length=20)
    slug = models.SlugField(max_length=20, unique=True) #ถ้าไม่ได้ใช้ก็ลบ เท่านั้น
    
    def __str__(self):
        return self.name
    
class LearningArea(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, unique=True) #ถ้าไม่ได้ใช้ก็ลบ เท่านั้น
    
    def __str__(self):
        return self.name
    
class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='comments')
    parent_content = models.ForeignKey(Content, on_delete=models.CASCADE, related_name='comments')
    body = models.CharField(max_length=150)
    likes = models.ManyToManyField(User, related_name="likedcomments", through="LikedComment")
    created = models.DateTimeField(auto_now_add=True)
    id = models.CharField(max_length=100, default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    
    def __str__(self):
        try:
            return f'{self.author.username} : {self.body[:30]}'
        except:
            return f'no author : {self.body[:30]}'
        
    class Meta:
        ordering = ['-created']

    
class LikedComment(models.Model):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f'{self.user.username}: {self.comment.body[:30]}'
        
class Reply(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='replies')
    parent_comment = models.ForeignKey(Comment, on_delete=models.CASCADE, related_name='replies')
    body = models.CharField(max_length=150)
    likes = models.ManyToManyField(User, related_name="likedreplies", through="LikedReply")
    created = models.DateTimeField(auto_now_add=True)
    id = models.CharField(max_length=100, default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    
    def __str__(self):
        try:
            return f'{self.author.username} : {self.body[:30]}'
        except:
            return f'no author : {self.body[:30]}'
        
    class Meta:
        ordering = ['created']
        
        
class LikedReply(models.Model):
    reply = models.ForeignKey(Reply, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f'{self.user.username}: {self.reply.body[:30]}'
    
class GalleryImage(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ImageField(upload_to='gallery/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Image for {self.content.title}"

class RichTextContent(models.Model):
    content = models.OneToOneField(Content, on_delete=models.CASCADE, related_name='rich_text_content')
    rich_text = CKEditor5Field('เนื้อหา', null=True, blank=True, config_name='extends')

    def __str__(self):
        #return self.rich_text[:50]  # แสดงเนื้อหาบางส่วนเพื่อให้ดูง่าย
        return str(self.content.title)
    
class Video(models.Model):
    content = models.OneToOneField(Content, on_delete=models.CASCADE, related_name='video')
    added = models.DateTimeField(auto_now_add=True)
    url = EmbedVideoField()
    
    def __str__(self):
        return str(self.content.title)
    
    #felt cute might delete later
    class Meta:
        ordering = ['added']

class EBook(models.Model):
    content = models.OneToOneField(Content, on_delete=models.CASCADE, primary_key=True)
    link = models.URLField(blank=True, null=True)
    file = models.FileField(null=True, blank=True, upload_to='ebooks/', validators=[FileExtensionValidator(['pdf', 'epub', 'mobi', 'azw', 'azw3', 'doc', 'docx'])])
    author = models.CharField(null=True, blank=True, max_length=255)
    publication_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.content.title
    
class Project(models.Model):
    content = models.OneToOneField(Content, on_delete=models.CASCADE, primary_key=True)
    # project_file = models.FileField(upload_to='projects/', validators=[FileExtensionValidator(['pdf'])], null=True)
    
    # Additional fields
    creators = models.CharField(max_length=255, null=True)  # Example: นาย ทศวิน จ้างประเสริฐ,นาย เหมชาติ เชื้อโชติ,นาย นิพัฒน์ จงเจริญศิริชื่อ
    advisor = models.CharField(max_length=255, null=True)  # Example: ดร. คมวุฒิ วิภูษิต
    institution = models.CharField(max_length=255, null=True)  # Example: สถาบันเทคโนโลยีนานาชาติสิรินธร มหาวิทยาลัยธรรมศาสตร์
    education_level = models.CharField(max_length=50, null=True)  # Example: ปริญญาโทขึ้นไป
    subject_category = models.CharField(max_length=100, null=True)  # Example: คอมพิวเตอร์
    completion_date = models.DateField(null=True)  # Example: 1 มกราคม 2541
    def __str__(self):
        return self.content.title
    
class ProjectFile(models.Model):
    project = models.ForeignKey(Project, related_name='files', on_delete=models.CASCADE)
    file = models.FileField(upload_to='projects/', validators=[FileExtensionValidator(['pdf'])])

    def __str__(self):
        return f"{self.project.content.title} - {self.file.name}"
    
class Application(models.Model):
    content = models.OneToOneField(Content, on_delete=models.CASCADE, primary_key=True)
    creator = models.CharField(max_length=255)
    link_playstore = models.URLField(blank=True, null=True)
    link_appstore = models.URLField(blank=True, null=True)
    
    def __str__(self):
        return self.content.title
    
class PlanFile(models.Model):
    # content = models.OneToOneField(Content, on_delete=models.CASCADE, primary_key=True)
    content = models.ForeignKey(Content, related_name='plan_files', on_delete=models.CASCADE )
    file = models.FileField(upload_to='teaching_plans/', validators=[FileExtensionValidator(['pdf','doc', 'docx'])], null=True, blank=True)

    def __str__(self):
        return f"{self.content.title} - {self.file.name}"
    
class SlideshowImage(models.Model):
    image = models.ImageField(upload_to='slideshow_images/')
    caption = models.CharField(max_length=255, blank=True, null=True)
    order = models.PositiveIntegerField(default=0)
    class Meta:
        ordering = ['order']

    def __str__(self):
        return f"Slideshow Image {self.id}"
