import os
import zipfile

from django.conf import settings
from urllib.parse import quote
from django.contrib import messages
from django.http import HttpResponse
from django.conf import settings
from django.db.models import Count
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required

from a_contents.utils.filters import ContentFilter

from django import get_version

from a_contents.utils.custom_validators import is_valid_image, is_valid_plan_file, is_valid_project_file
from a_contents.utils.video_thumbnail import save_thumbnail

if get_version() >= "4.0":
    from django.utils.translation import gettext_lazy as _
else:
    from django.utils.translation import ugettext_lazy as _
# from django_ckeditor_5.forms import UploadFileForm
from django_ckeditor_5.views import image_verify, handle_uploaded_file
from django.http import JsonResponse
from django.http import Http404
class NoImageException(Exception):
    pass

from .forms import *
from .models import *

from django.http import Http404
from django.utils.module_loading import import_string


from django.conf import settings
# from django.core.exceptions import ImproperlyConfigured
from django.http import JsonResponse
# from PIL import Image

#การหาความใกล้เคียง
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models import Q, Case, When, IntegerField

#page paginate
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import F, Q

# Create your views here.
def home_view(request, content_type=None):
    # กำหนด queryset เบื้องต้น
    slideshow_images = SlideshowImage.objects.all()
    queryset = Content.objects.filter(approval_status=Content.ApprovalStatus.APPROVED)
    
    # กรองข้อมูลตาม content_type ถ้ามี
    if content_type:
        queryset = queryset.filter(type=content_type)
        template = 'a_contents/query_page.html'
        partial_template = 'partials/query_content_list.html'
    else:
        template = 'a_contents/home.html'
        partial_template = 'partials/home_content_list.html'
    
    # สร้าง content_filter ก่อนการจัดเรียง
    content_filter = ContentFilter(request.GET, queryset=queryset)
    
    # การจัดเรียงข้อมูลตาม sort_order
    sort_order = request.GET.get('sort', 'popular')
    if sort_order == 'popular':
        queryset = content_filter.qs.order_by('-view_count', '-likes')
    elif sort_order == 'latest':
        queryset = content_filter.qs.order_by('-created')
    
    # การตั้งค่า Pagination
    paginator = Paginator(queryset, 12)  # แสดง 12 รายการต่อหน้า
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    # คำนวณค่าของฟอร์มกรอง
    form = content_filter.form
    
    # สร้าง URL สำหรับการส่งกลับไป
    base_url = request.path
    query_params = request.GET.copy()
    query_params['sort'] = sort_order
    query_string = query_params.urlencode()
    full_url = f"{base_url}?{query_string}"
    
    context = {
        'slideshow_images': slideshow_images,
        'contents': page_obj,
        'form': form,
        'content_type': content_type,
        'sort_order': sort_order,
        'full_url': full_url,  # ส่ง URL กลับไปที่ template
    }

    # ใช้ HTMX เพื่อโหลด partials
    if request.htmx:
        return render(request, partial_template, context)
    
    return render(request, template, context)

@login_required
def content_create_view(request, content_type):
  form = ContentMetaForm()
  ckeditor_form = None
  video_form = None
  ebook_form = None
  project_form = None
  app_form = None
  # teaching_plan_form = None
  #the form is different for each content type
  if content_type == 'article':
    ckeditor_form = RichTextContentForm()
    template = 'a_contents/content_create_article.html'
  elif content_type == 'video':
    video_form = VideoForm()
    template = 'a_contents/content_create_video.html'
  elif content_type == 'ebook':
    ebook_form = EBookForm()
    template = 'a_contents/content_create_ebook.html'
  elif content_type == 'project':
    project_form = ProjectForm()
    template = 'a_contents/content_create_project.html'
  elif content_type == 'app':
    app_form = AppForm()
    template = 'a_contents/content_create_app.html'
  elif content_type == 'gallery':
    template = 'a_contents/content_create_gallery.html'
  elif content_type == 'teaching_plan':
    # teaching_plan_form = TeachingPlanForm()
    template = 'a_contents/content_create_teaching_plan.html'
  else:
    return redirect('home')
  
  #save the content
  if request.method == "POST":
      form = ContentMetaForm(request.POST, request.FILES)
      if form.is_valid():
        print("form is valid")
        content = form.save(commit=False)
        content.author = request.user
        if content_type == 'article':
          ckeditor_form = RichTextContentForm(request.POST, request.FILES)
          if ckeditor_form.is_valid():
            content.type = 'R'
            content.save()
            form.save_m2m()
            #save the rich text content
            # ckeditor_form = RichTextContentForm(request.POST, request.FILES)
            rich_text_content = ckeditor_form.save(commit=False)
            rich_text_content.content = content
            rich_text_content.save()
          
        elif content_type == 'gallery':
          images = request.FILES.getlist('images')
          for image in images:
            is_valid, error_message = is_valid_image(image)
            if not is_valid:
                # print(f"Image validation failed: {error_message}")
                messages.error(request, f"{error_message}")
                return redirect('content-create', content_type='gallery')
          content.type = 'G'
          content.save()
          form.save_m2m()
          #save the images
          for image in images:
            GalleryImage.objects.create(content=content, image=image)

        elif content_type == 'video':
          video_form = VideoForm(request.POST)
          if video_form.is_valid():
            content.type = 'V'
            content.save()
            form.save_m2m()
            #save url video
            video = video_form.save(commit=False)
            video.content = content
            video.save()
            
            save_thumbnail(content, video_form.cleaned_data['url'])
            content.save()
          else:
            # print(video_form.errors)
            messages.error(request, 'ลิงค์ไม่ถูกต้อง')
            return redirect('content-create', content_type='video')
            
        elif content_type == 'ebook':
          ebook_form = EBookForm(request.POST, request.FILES)
          if ebook_form.is_valid():
            content.type = 'E'
            content.save()
            form.save_m2m()
            #save ebook
            ebook = ebook_form.save(commit=False)
            ebook.content = content
            ebook.save()
          else:
            messages.error(request, 'ไฟล์ไม่ถูกต้อง')
            return redirect('content-create', content_type='ebook')
          
        elif content_type == 'project':
          # print("content_type == project")
          project_form = ProjectForm(request.POST)
          files = request.FILES.getlist('project-files')
          for file in files:
            is_valid, error_message = is_valid_project_file(file)
            if not is_valid:
                print(f"file validation failed: {error_message}")
                messages.error(request, f"{error_message}")
                return redirect('content-create', content_type='project')
          if project_form.is_valid():
            content.type = 'P'
            content.save()
            form.save_m2m()
            
            #save project and files
            project = project_form.save(commit=False)
            project.content = content
            project = project_form.save()
            for file in files:
              ProjectFile.objects.create(project=project, file=file)
              
        elif content_type == 'app':
          app_form = AppForm(request.POST)
          if app_form.is_valid():
            content.type = 'A'
            content.save()
            form.save_m2m()
            #save url store
            app = app_form.save(commit=False)
            app.content = content
            app.save()
          else:
            messages.error(request, 'ลิงค์ถูกต้อง')
            return redirect('content-create', content_type='ebook')
        
        elif content_type == 'teaching_plan':
          files = request.FILES.getlist('plan-files')
          for file in files:
            is_valid, error_message = is_valid_plan_file(file)
            if not is_valid:
                print(f"file validation failed: {error_message}")
                messages.error(request, f"{error_message}")
                return redirect('content-create', content_type='teaching_plan')
          content.type = 'T'
          content.save()
          form.save_m2m()
            
          #save files
          for file in files:
            PlanFile.objects.get_or_create(content=content, file=file)
              
        messages.success(request, 'สร้างเนื้อหาสำเร็จ')
        return redirect('content', pk=content.pk) 
  #show the form
  context = {
    'form': form,
    'content_type': content_type,
    'ckeditor_form': ckeditor_form,
    'video_form': video_form,
    'ebook_form': ebook_form,
    'project_form': project_form,
    'app_form': app_form,
  }
  return render(request, template, context)

@login_required
def content_delete_view(request, pk):
  content = get_object_or_404(Content, id=pk, author=request.user)
    
  if request.method == 'POST':
    # ลบรูปภาพที่เชื่อมโยงกับเนื้อหานี้
    if content.type == 'G':
      images = GalleryImage.objects.filter(content=content)
      for image in images:
        # ลบไฟล์รูปภาพจากระบบไฟล์
        if image.image and os.path.isfile(image.image.path):
          os.remove(image.image.path)
        image.delete()
        
    #ลบรูปหน้าปก
    if content.image and os.path.isfile(content.image.path):
      os.remove(content.image.path)
    # ลบเนื้อหา
    content.delete()
    messages.success(request, 'ลบเนื้อหาสำเร็จ')
    return redirect('home')
  return render(request, 'a_contents/content_delete.html', { 'content':content })

@login_required
def content_edit_view(request, pk):
  content = get_object_or_404(Content, id=pk, author=request.user)
  form = ContentMetaForm(instance=content)
  ckeditor_form = None
  video_form = None
  ebook_form = None
  project_form = None
  app_form = None

  if content.type == 'R':
    ckeditor_form = RichTextContentForm(instance=content.rich_text_content)
    
  elif content.type == 'V':
    video_form = VideoForm(instance=content.video)
    
  elif content.type == 'E':
    ebook_form = EBookForm(instance=content.ebook)
    
  elif content.type == 'P':
    project_form = ProjectForm(instance=content.project)
    
  elif content.type == 'A':
    app_form = AppForm(instance=content.application)
  
  #save the content
  if request.method == 'POST':
    form = ContentMetaForm(request.POST, request.FILES, instance=content)
    if content.type == 'R':
      ckeditor_form = RichTextContentForm(request.POST, request.FILES, instance=content.rich_text_content)
      if form.is_valid() and ckeditor_form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        ckeditor_form.save()
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk)
      
    elif content.type == 'G':
      if form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        #save new image and delete old images
        try:
          images = request.FILES.getlist('images')
        except:
          images = None
        if images:
          #validate images
          for image in images:
            is_valid, error_message = is_valid_image(image)
            if not is_valid:
                # print(f"Image validation failed: {error_message}")
                messages.error(request, f"{error_message}")
                return redirect('content-edit', pk=content.pk)
          #delete old images
          old_images = GalleryImage.objects.filter(content=content)
          for image in old_images:
            # ลบไฟล์รูปภาพจากระบบไฟล์
            if image.image and os.path.isfile(image.image.path):
              os.remove(image.image.path)
              image.delete()
          # new images
          for image in images:
            GalleryImage.objects.create(content=content, image=image)
        else:
          messages.success(request, 'ไม่มีรูป')
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk)
      
    elif content.type == 'V':
      video_form = VideoForm(request.POST, instance=content.video)
      if form.is_valid() and video_form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        video_form.save()
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk)
      
    elif content.type == 'E':
      ebook_form = EBookForm(request.POST, request.FILES, instance=content.ebook)
      if form.is_valid() and ebook_form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        ebook_form.save()
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk)
      
    elif content.type == 'P':
      project_form = ProjectForm(request.POST, instance=content.project)
      if form.is_valid() and project_form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        #save new image and delete old images
        try:
          files = request.FILES.getlist('project-files')
        except:
          files = None
        if files:
          #validate file
          for file in files:
            is_valid, error_message = is_valid_project_file(file)
            if not is_valid:
                # print(f"Image validation failed: {error_message}")
                messages.error(request, f"{error_message}")
                return redirect('content-edit', pk=content.pk)
          #delete old files
          old_files = ProjectFile.objects.filter(project=content.project)
          for file in old_files:
            # ลบไฟล์รูปภาพจากระบบไฟล์
            if file.file and os.path.isfile(file.file.path):
              os.remove(file.file.path)
              file.delete()
          # new images
          for file in files:
            ProjectFile.objects.create(project=content.project, file=file)
        else:
          messages.success(request, 'ไม่มีไฟล์ pdf')
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk)
      
    elif content.type == 'A':
      app_form = AppForm(request.POST, instance=content.application)
      if form.is_valid() and app_form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        app_form.save()
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk) 
       
    elif content.type == 'T':
      # project_form = ProjectForm(request.POST, instance=content.project)
      if form.is_valid():
        if content.approval_status == 'R':
          content.approval_status = 'P'
        form.save()
        #save new file and delete old one
        try:
          files = request.FILES.getlist('plan-files')
        except:
          files = None
        if files:
          #validate file
          for file in files:
            is_valid, error_message = is_valid_plan_file(file)
            if not is_valid:
                # print(f"Image validation failed: {error_message}")
                messages.error(request, f"{error_message}")
                return redirect('content-edit', pk=content.pk)
          #delete old files
          old_files = PlanFile.objects.filter(content=content)
          for file in old_files:
            # ลบไฟล์จากระบบไฟล์
            if file.file and os.path.isfile(file.file.path):
              os.remove(file.file.path)
              file.delete()
          # new file
          for file in files:
            PlanFile.objects.create(content=content, file=file)
        else:
          messages.success(request, 'ไม่มีไฟล์ pdf')
        messages.success(request, 'แก้ไขสำเร็จ')
        return redirect('content', pk=content.pk)  
        
  context = {
    'content': content,
    'form': form,
    'ckeditor_form': ckeditor_form,
    'video_form': video_form,
    'ebook_form': ebook_form,
    'project_form': project_form,
    'app_form': app_form,
  }
  return render(request, 'a_contents/content_edit.html', context)

def content_page_view(request, pk):
  content = get_object_or_404(Content, id=pk)
  
  # แยกคำใน title และ description
  title_keywords = content.title.split()
  description_keywords = content.description.split()
  
  # สร้าง Q object สำหรับการค้นหาหลายคำใน title และ description
  title_query = Q()
  description_query = Q()
  for keyword in title_keywords:
    title_query |= Q(title__icontains=keyword)
  for keyword in description_keywords:
    description_query |= Q(description__icontains=keyword)
    
  # เพิ่มเงื่อนไขการค้นหาจาก learning_areas
  learning_areas_query = Q()
  if content.learning_areas.exists():
    learning_areas_query |= Q(learning_areas__in=content.learning_areas.all())
  
  # ค้นหาเนื้อหาที่มีคำสำคัญหรือคำค้นหาที่คล้ายกัน
  related_contents = Content.objects.filter(
    title_query | description_query | learning_areas_query
).exclude(id=content.id).filter(
    approval_status=Content.ApprovalStatus.APPROVED
).distinct()

  # จัดลำดับความสำคัญให้กับ title และ description ก่อน
  related_contents = related_contents.annotate(
    priority=Case(
      When(title_query, then=2),
      When(description_query, then=1),
      default=0,
      output_field=IntegerField(),
    )
  ).order_by('-priority', '-view_count', '-likes')[:3]  # จำกัดจำนวนผลลัพธ์ที่แสดงเป็น 3 รายการ
  
  # Increment view count
  # ตรวจสอบ IP
  ip = request.META.get('REMOTE_ADDR')
  view_key = f'viewed_{pk}_{ip}'
  
  if not request.session.get(view_key):
   content.view_count += 1
   content.save()
   request.session[view_key] = True
  
  commentform = CommentCreateForm()
  replyform = ReplyCreateForm()
  
  #comment
  if request.htmx:
    if 'top' in request.GET:
      comments = content.comments.annotate(num_likes=Count('likes')).filter(num_likes__gt=0).order_by('-num_likes')
    else:
      comments = content.comments.all()
    return render(request, 'snippets/loop_contentpage_comments.html', {'comments': comments, 'replyform':replyform})
  
  content_filter = ContentFilter(request.GET, queryset=related_contents)
  
  context = {
    'content' : content,
    'related_contents': content_filter.qs,
    'commentform': commentform,
    'replyform': replyform
  }
  
  return render(request, 'a_contents/content_page.html', context)


def download_gallery_images(request, content_id):
    content = get_object_or_404(Content, id=content_id)
    images = content.gallery_images.all()
    
    # Define the directory for ZIP files
    zip_dir = os.path.join(settings.MEDIA_ROOT, 'image_zip')
    os.makedirs(zip_dir, exist_ok=True)  # Create the directory if it doesn't exist

    zip_filename = f"{content.title.replace(' ', '_')}_images.zip"
    zip_filepath = os.path.join(zip_dir, zip_filename)
    
    # Create the ZIP file
    with zipfile.ZipFile(zip_filepath, 'w') as zip_file:
        for image in images:
            image_path = image.image.path
            arcname = os.path.basename(image_path)
            zip_file.write(image_path, arcname)
    
    # Send the ZIP file as a response
    with open(zip_filepath, 'rb') as zip_file:
        response = HttpResponse(zip_file.read(), content_type='application/zip')
        content_disposition = f'attachment; filename="{zip_filename}"; filename*=UTF-8\'\'{quote(zip_filename)}'
        response['Content-Disposition'] = content_disposition
    
    # Delete the ZIP file after sending the response
    os.remove(zip_filepath)
    
    return response
  
#ckeditor5 custom upload and will fix it with delete image later
def upload_file(request):
    # if request.method == "POST" and request.user.is_staff:
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        print(form.is_valid())
        allow_all_file_types = getattr(settings, "CKEDITOR_5_ALLOW_ALL_FILE_TYPES", False)

        if not allow_all_file_types:
            try:
                image_verify(request.FILES['upload'])
            except NoImageException as ex:
                return JsonResponse({"error": {"message": f"{ex}"}}, status=400)
        if form.is_valid():
            url = handle_uploaded_file(request.FILES["upload"])
            return JsonResponse({"url": url})
        else:
            print(form.errors)
    raise Http404(_("Page not found."))

@login_required
def comment_sent(request, pk):
  content = get_object_or_404(Content, id=pk)
  replyform = ReplyCreateForm()
  
  if request.method == 'POST':
    form = CommentCreateForm(request.POST)
    if form.is_valid:
      comment = form.save(commit=False)
      comment.author = request.user
      comment.parent_content = content
      comment.save()
      
  context = {
    'comment': comment,
    'replyform': replyform,
    'content': content
  }
      
  return render(request, 'snippets/add_comment.html', context)

@login_required
def reply_sent(request, pk):
  comment = get_object_or_404(Comment, id=pk)
  replyform = ReplyCreateForm()
  
  if request.method == 'POST':
    form = ReplyCreateForm(request.POST)
    if form.is_valid:
      reply = form.save(commit=False)
      reply.author = request.user
      reply.parent_comment = comment
      reply.save()
      
  context = {
    'reply': reply,
    'comment': comment,
    'replyform': replyform
  }   
      
  return render(request, 'snippets/add_reply.html', context)

@login_required
def comment_delete_view(request, pk):
  content = get_object_or_404(Comment, id=pk, author=request.user)
  
  if request.method == "POST":
    content.delete()
    messages.success(request, 'ลบความคิดเห็นเรียบร้อย')
    return redirect('content', content.parent_content.id)
  
  return render(request, 'a_contents/comment_delete.html', {'comment': content})

@login_required
def reply_delete_view(request, pk):
  reply = get_object_or_404(Reply, id=pk, author=request.user)
  
  if request.method == "POST":
    reply.delete()
    messages.success(request, 'ลบการตอบกลับเรียบร้อย')
    return redirect('content', reply.parent_comment.parent_content.id)
  
  return render(request, 'a_contents/reply_delete.html', {'reply': reply})


def like_toggle(model):
  def inner_func(func):
    def wrapper(request, *args, **kwargs):
      content = get_object_or_404(model, id=kwargs.get('pk'))
      user_exist = content.likes.filter(username=request.user.username).exists()
      # author of the content can't like their if use this
      # if content.user != request.user:
      if user_exist:
        content.likes.remove(request.user)
      else:  
        content.likes.add(request.user)
      return func(request, content)
    return wrapper
  return inner_func 

@login_required
@like_toggle(Content)
def like_content(request, content):
  return render(request, 'snippets/likes.html', {'content': content})

@login_required
@like_toggle(Comment)
def like_comment(request, content):
  return render(request, 'snippets/likes_comment.html', {'comment': content})


@login_required
@like_toggle(Reply)
def like_reply(request, content):
  return render(request, 'snippets/likes_reply.html', {'reply': content})

#slideshow
def slideshow_view(request):
    slideshow_images = SlideshowImage.objects.all()
    return render(request, 'your_template.html', {'slideshow_images': slideshow_images})
