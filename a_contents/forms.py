from django.forms import ModelForm, modelformset_factory
from django import forms
from django.core.exceptions import ValidationError
import re

from django_ckeditor_5.widgets import CKEditor5Widget

from django.conf import settings
from django.core.validators import FileExtensionValidator

from .models import *

class ContentMetaForm(ModelForm):
  def __init__(self, *args, **kwargs):
          super().__init__(*args, **kwargs)
          self.fields["description"].required = False
  class Meta:
    model = Content
    fields = '__all__'
    exclude = ['type','author','likes','comments', 'view_count', 'approval_status']
    labels = {
      "title": 'ชื่อเรื่อง *',
      "image": "ภาพหน้าปก",
      "description": "คำอธิบาย *",
      "grades": "ชั้นเรียน *",
      "learning_areas": "สาระการเรียนรู้ *",
      "subject": "ชื่อวิชา",
    }
    widgets = {
      'image' : forms.FileInput(),
      'description': forms.Textarea(attrs={'rows':3,'class': 'form-control', 'placeholder': 'รายละเอียด'}),
      "grades": forms.CheckboxSelectMultiple(),
      "learning_areas": forms.CheckboxSelectMultiple(),
    }
    
class RichTextContentForm(forms.ModelForm):
  class Meta:
    model = RichTextContent
    fields = ['rich_text']
    labels = {
      "rich_text": 'เนื้อหา *',
    }
    widgets = {
      "rich_text": CKEditor5Widget(attrs={"class": "django_ckeditor_5"}, config_name="extends"),
    }

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['url']
        labels = {"url": 'วิดีโอ *',}
        widgets = {
            'url': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'ใส่ลิงก์วิดีโอ'})
        }
    def clean_video_url(self):
        video_url = self.cleaned_data['video_url']
        # Regular expression pattern for YouTube URL validation
        youtube_regex = r'^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$'
        if not re.match(youtube_regex, video_url):
            raise ValidationError("Invalid YouTube URL")
        return video_url
      
class EBookForm(forms.ModelForm):
    # publication_date = forms.DateField(
    #     widget=forms.DateInput(attrs={'type': 'month'}),
    #     input_formats=['%Y-%m'],
    #     label="วันที่เผยแพร่ *"
    # )
    class Meta:
        model = EBook
        fields = ['link', 'file', 'author']
        labels = {
        "link": 'ลิงก์ E-Book/Canva',
        "file": "ไฟล์ E-Book *",
        "author": "ผู้แต่ง *",
        }
    # class Meta:
    #     model = EBook
    #     fields = ['link']
    #     labels = {
    #     "link": 'ลิงก์ E-Book',
    #     }
        
class ProjectForm(forms.ModelForm):
  completion_date = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'month'}),
        input_formats=['%Y-%m'],
        label="ทำโครงงาน/วิจัยเมื่อ *"
    )
  class Meta:
    model = Project
    fields = [
    'creators', 
    'advisor', 
    'institution', 
    'education_level', 
    'subject_category', 
    'completion_date'
    ]
    labels = {
      "creators": 'ผู้จัดทำ *',
      "advisor": "อาจารย์ที่ปรึกษา *",
      "institution": "สถาบันการศึกษา *",
      "education_level": "ระดับชั้น *",
      "subject_category": "หมวดวิชา *",
    }
    
class AppForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ['creator', 'link_playstore', 'link_appstore']
        labels = {
          "creators": 'ผู้จัดทำ *',
          "link_playstore": "ลิงก์ Playstore",
          "link_appstore": "ลิงก์ Appstore",
        }

class CommentCreateForm(ModelForm):
  class Meta:
    model = Comment
    fields = ['body']
    widgets = {
      'body' : forms.TextInput(attrs={'placeholder': 'เขียนความคิดเห็น ...'})
    }
    labels = {
      'body': ''
    }

class ReplyCreateForm(ModelForm):
  class Meta:
    model = Reply
    fields = ['body']
    widgets = {
      'body' : forms.TextInput(attrs={'placeholder': 'เขียนการตอบกลับ ...', 'class':"!text-sm"})
    }
    labels = {
      'body': ''
    }

class UploadFileForm(forms.Form):
    upload = forms.FileField(
        validators=[
            FileExtensionValidator(
                getattr(
                    settings,
                    "CKEDITOR_5_UPLOAD_FILE_TYPES",
                    ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'webp', 'tiff'],
                ),
            ),
        ],
    )

  