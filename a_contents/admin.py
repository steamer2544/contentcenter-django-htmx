from django.contrib import admin
from django.urls import path
from django.utils.html import format_html
from embed_video.admin import AdminVideoMixin
from .models import *

class GalleryImageInline(admin.StackedInline):
    model = GalleryImage
    extra = 1
    can_delete = False

class AdminVideo(AdminVideoMixin, admin.ModelAdmin):
    pass
class VideoInline(AdminVideoMixin, admin.StackedInline):
    model = Video
    extra = 1
    can_delete = False

class AppInline(admin.StackedInline):
    model = Application
    extra = 1
    can_delete = False

class RichTextContentInline(admin.StackedInline):
    model = RichTextContent
    extra = 1
    can_delete = False
    
class ProjectInline(admin.StackedInline):
    model = Project
    extra = 1
    can_delete = False
    
class PlanFileInline(admin.StackedInline):
    model = PlanFile
    extra = 1
    can_delete = False

@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    inlines = []
    
    list_display = ('title', 'type', 'author', 'approval_status', 'created')
    list_filter = ('approval_status', 'type', 'created')
    search_fields = ('title', 'description', 'author__username')
    actions = ['approve_content', 'reject_content']

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return []

        inlines = []

        if obj.type == Content.TypeChoices.VIDEO:
            inlines.append(VideoInline)
        elif obj.type == Content.TypeChoices.GALLERY:
            inlines.append(GalleryImageInline)
        elif obj.type == Content.TypeChoices.ARTICLE:
            inlines.append(RichTextContentInline)
        elif obj.type == Content.TypeChoices.APP:
            inlines.append(AppInline)
        elif obj.type == Content.TypeChoices.PROJECT:
            inlines.append(ProjectInline)
        elif obj.type == Content.TypeChoices.TEACHING_PLAN:
            inlines.append(PlanFileInline)
        # Add more conditions for other content types if necessary

        return [inline(self.model, self.admin_site) for inline in inlines]
    
    def approve_content(self, request, queryset):
        queryset.update(approval_status=Content.ApprovalStatus.APPROVED)
        self.message_user(request, "Selected content has been approved.")
    approve_content.short_description = "Approve selected content"
    
    def reject_content(self, request, queryset):
        queryset.update(approval_status=Content.ApprovalStatus.REJECTED)
        self.message_user(request, "Selected content has been rejected.")
    reject_content.short_description = "Reject selected content"
    
@admin.register(SlideshowImage)
class SlideshowImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'caption', 'order')
    list_editable = ('caption', 'order')

# admin.site.register(Content, ContentAdmin)
admin.site.register(Grade)
admin.site.register(LearningArea)
admin.site.register(Comment)
admin.site.register(Reply)
admin.site.register(LikedContent)
admin.site.register(LikedComment)
admin.site.register(LikedReply)

# admin.site.register(RichTextContent)
# admin.site.register(GalleryImage)
# admin.site.register(Video, AdminVideo)
# admin.site.register(Project)
# admin.site.register(Application)
